#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
# @Time   : 2022/3/28 13:22
# @Author : 李晓杰
"""
import os


def get_all_files(file_path, yaml_data_switch=False) -> list:
    """
        获取文件绝对路径
    :param file_path: 目录路径
    :param yaml_data_switch: 是否过滤文件为 yaml格式， True则过滤
    :return:
    os.walk():创建一个生成器，用以生成所要查找的目录及其子目录下的所有文件
    rootF:\JACK\My_project\pytest-auto-api2\data
    dirs['Collect', 'JekunAuto_WeChat', 'Login', 'UserInfo']
    files['__init__.py']。。。。
    """
    filename = []
    # 获取所有文件下的子文件名称
    for root, dirs, files in os.walk(file_path):
        for _file_path in files:
            path = os.path.join(root, _file_path)
            if yaml_data_switch:
                if 'yaml' in path or '.yml' in path:
                    filename.append(path)
            else:
                filename.append(path)
    return filename


if __name__ == '__main__':
    print(get_all_files("F:\\JACK\\My_project\\pytest-auto-api2\\data\\JekunAuto_WeChat",))