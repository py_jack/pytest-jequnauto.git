#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2023-03-10 11:03:09

import allure
import pytest
from utils.read_files_tools.get_yaml_data_analysis import GetTestCase
from utils.assertion.assert_control import Assert
from utils.requests_tool.request_control import RequestControl
from utils.read_files_tools.regular_control import regular
from utils.requests_tool.teardown_control import TearDownHandler
import pytest_rerunfailures

case_id = ['platform_index_01', 'platform_property_02', 'platform_goods_list_03', 'platform_goods_comment_04', 'platform_goods_total_05']
TestData = GetTestCase.case_data(case_id)
re_data = regular(str(TestData))


@allure.epic("JekunAuto小程序接口")
@allure.feature("商城模块")
class TestPlatformIndex:

    @allure.story("商城模块")
    @pytest.mark.flaky(reruns=3, reruns_delay=3)
    @pytest.mark.parametrize('in_data', eval(re_data), ids=[i['detail'] for i in TestData])
    def test_platform_index(self, in_data, case_skip):
        """
        :param :
        :return:
        """
        res = RequestControl(in_data).http_request()
        TearDownHandler(res).teardown_handle()
        Assert(in_data['assert_data']).assert_equality(response_data=res.response_data,
                                                       sql_data=res.sql_data, status_code=res.status_code)


if __name__ == '__main__':
    pytest.main(['test_platform_index.py', '-s', '-W', 'ignore:Module already imported:pytest.PytestWarning'])
