#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2022/3/28 15:28
# @Author : 李晓杰
from common.setting import ensure_path_sep
from utils.read_files_tools.get_yaml_data_analysis import CaseData
from utils.read_files_tools.get_all_files_path import get_all_files
from utils.cache_process.cache_control import CacheHandler, _cache_config


def write_case_process():
    """
    获取所有用例，写入用例池中_cache_config = {}
    :return:
    1. 通过CaseData已经拿到了yaml数据，返回的是一个list[]，里面装一个个字典[{'wechat_login_001': {'url':.....},{'wechat_login_002
    2. 遍历列表，拿出一个个字典,一个字典就是一条测试用例
    3. 判断缓存池_cache_config有没有这个字典，没有就调用CacheHandler.update_cache添加进去
    4. 注意case_process 存的是一个yaml文件的列表，_cache_config存的是所有yaml文件的字典
    """
    # 默认都是拿YAML
    # 循环拿到所有存放用例的文件路径
    for i in get_all_files(file_path=ensure_path_sep("\\data"), yaml_data_switch=True):
        # 循环读取文件中的数据
        # 返回一个list[] [{'wechat_login_001': {'url':.....},{'wechat_login_002
        case_process = CaseData(i).case_process(case_id_switch=True)
        if case_process is not None:
            # 转换数据类型
            for case in case_process:
                for k, v in case.items():
                    # 判断 case_id 是否已存在
                    case_id_exit = k in _cache_config.keys()
                    # 如果case_id 不存在，则将用例写入缓存池中
                    if case_id_exit is False:
                        CacheHandler.update_cache(cache_name=k, value=v)
                        # case_data[k] = v
                    # 当 case_id 为 True 存在时，则跑出异常
                    elif case_id_exit is True:
                        raise ValueError(f"case_id: {k} 存在重复项, 请修改case_id\n"
                                         f"文件路径: {i}")

write_case_process()
